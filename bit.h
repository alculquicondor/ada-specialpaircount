#ifndef BIT_H_
#define BIT_H_

class BinIdxTree {
 private:
  int *BIT, n;

 public:
  BinIdxTree(int n) : n(n) {
    BIT = new int[n+1];
  }
  ~BinIdxTree() {
    delete[] BIT;
  }

  inline int query(int idx) {
    int ans = 0;
    while (idx) {
      ans += BIT[idx];
      idx -= idx & -idx;
    }
    return ans;
  }

  void update(int idx, int add = 1) {
    while (idx <= n) {
      BIT[idx] += add;
      idx += idx & -idx;
    }
  }
};

#endif /* end of include guard: BIT_H_ */
