#ifndef SEG_TREE_H_
#define SEG_TREE_H_

#include <limits>
#include <algorithm>

typedef std::size_t size_t;

inline size_t center(int x, int y) {
  return (x+y) >> 1;
}
inline size_t left(int x) {
  return (x<<1) + 1;
}
inline size_t logCeil(int x) {
  return 32 - __builtin_clz(x - 1);
}
const size_t INF = std::numeric_limits<size_t>::max();

class MinSegTree {
 private:
  size_t n, *st;
 public:
  MinSegTree(size_t n, size_t val) : n(n) {
    size_t sz = 1 << (logCeil(n) + 1);
    st = new size_t[sz];
    std::fill(st, st+sz, val);
  }
  ~MinSegTree() {
    delete[] st;
  }

  void update(size_t i, int up, size_t idx, size_t l, size_t r) {
    if (r < i or l > i)
      return;
    if (l == r) {
      st[idx] = up;
      return;
    }
    size_t L = left(idx), R = L + 1, mid = center(l, r);
    update(i, up, L, l, mid);
    update(i, up, R, mid + 1, r);
    st[idx] = std::min(st[L], st[R]);
  }
  void update(size_t i, int up) {
    update(i, up, 0, 0, n - 1);
  }

  size_t query(size_t i, size_t j, size_t idx, size_t l, size_t r) {
    if (l >= i and r <= j)
      return st[idx];
    if (r < i or l > j)
      return INF;
    size_t L = left(idx), R = L + 1, mid = center(l, r);
    return std::min(query(i, j, L, l, mid), query(i, j, R, mid+1, r));
  }
  int query(size_t i, size_t j) {
    return query(i, j, 0, 0, n - 1);
  }

};


class SumSegTree {
 private:
  size_t *st;
  bool *add;
  int n;

 public:
  SumSegTree(size_t n, size_t val) : n(n) {
    size_t sz = 1 << (logCeil(n) + 1);
    st = new size_t[sz];
    add = new bool[sz];
    std::fill(st, st + sz, val);
    std::fill(add, add + sz, false);
  }
  ~SumSegTree() {
    delete[] st;
    delete[] add;
  }

  inline void push(size_t idx, size_t l, size_t r, size_t L, size_t R) {
    if (add[idx]) {
      st[idx] = 0;
      if (l != r) {
        add[L] = true;
        add[R] = true;
      }
      add[idx] = false;
    }
  }

  void clear(size_t i, size_t j, size_t idx, size_t l, size_t r) {
    size_t L = left(idx), R = L + 1;
    if (l >= i and r <= j)
      add[idx] = true;
    push(idx, l, r, L, R);
    if ((l >= i and r <= j) or r < i or l > j)
      return;
    size_t mid = center(l, r);
    clear(i, j, L, l, mid);
    clear(i, j, R, mid + 1, r);
    st[idx] = st[L] + st[R];
  }
  void clear(size_t i, size_t j) {
    clear(i, j, 0, 0, n - 1);
  }

  void set(size_t i, size_t idx, size_t l, size_t r) {
    size_t L = left(idx), R = L + 1;
    push(idx, l, r, L, R);
    if (l >= i and r <= i) {
      st[idx] = 1;
      return;
    }
    if (r < i or l > i)
      return;
    size_t mid = center(l, r);
    set(i, L, l, mid);
    set(i, R, mid + 1, r);
    st[idx] = st[L] + st[R];
  }
  void set(size_t i) {
    set(i, 0, 0, n - 1);
  }

  size_t query(size_t i, size_t j, size_t idx, size_t l, size_t r) {
    size_t L = left(idx), R = L + 1;
    push(idx, l, r, L, R);
    if (l >= i and r <= j)
      return st[idx];
    if (r < i or l > j)
      return 0;
    size_t mid = center(l, r);
    return query(i, j, L, l, mid) + query(i, j, R, mid+1, r);
  }
  size_t query(size_t i, size_t j) {
    return query(i, j, 0, 0, n - 1);
  }

};

#endif /* end of include guard: SEG_TREE_H_ */
