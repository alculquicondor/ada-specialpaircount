#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <chrono>

#include "seg_tree.h"

using namespace std;

typedef pair<int, size_t> pii;

vector<int> read() {
  size_t n;
  scanf("%lu", &n);
  vector<int> A(n);
  for (size_t i = 0; i < n; ++i)
    scanf("%d", &A[i]);
  return A;
}

size_t countSpecial(const vector<int> &A) { // O(n)
  size_t ans = 0;
  for (size_t i = 0; i < A.size(); ++i) {
    size_t j;
    int maxi = A[i];
    for (j = i + 1; j < A.size() && A[j] > A[i]; ++j)
      if (A[j] > maxi) {
        ++ans;
        maxi = A[j];
      }
  }
  return ans;
}

class SpecialPairsCounter { // O(n * lg n)
 private:
  vector<int> values;

  void buildValues(const vector<int> &A) {
    vector<int> &V = values = A;
    sort(V.begin(), V.end());
    V.resize(unique(V.begin(), V.end()) - V.begin());
  }

  size_t getPosition(int x) {
    return lower_bound(values.begin(), values.end(), x) - values.begin();
  }

 public:
  size_t operator()(const vector<int> &A) {
    size_t ans = 0, r, p;
    SumSegTree cnt(A.size(), 0);
    buildValues(A);
    MinSegTree pos(values.size()+1, A.size());
    p = getPosition(A[A.size()-1]);
    pos.update(p, A.size() - 1);
    for (int i = A.size() - 2; i >= 0; --i) {
      p = getPosition(A[i]);
      r = pos.query(p+1, values.size()+1);
      cnt.clear(i, r-1);
      if (A[i+1] > A[i])
        cnt.set(i+1);
      r = pos.query(0, p);
      ans += cnt.query(i, r-1);
      pos.update(p, i);
    }
    return ans;
  }

};

struct descriptor {
  int value;
  size_t size;
};

size_t countSpecialN(const vector<int> &A) {
  if (A.empty())
    return 0;
  vector<int> increasing(1, A.front());
  vector<descriptor> stops(1, {A.front(), 0});
  size_t ans = 0, size_indicator;
  for (size_t i = 1; i < A.size(); ++i) {
    while (increasing.size() and A[i] <= increasing.back())
      increasing.pop_back();
    while (stops.size() and A[i] > stops.back().value) {
      size_t t = stops.back().size;
      stops.pop_back();
      if (stops.size() and stops.back().size > t)
        stops.back().size = t;
    }
    size_indicator = stops.size() ?
      min(increasing.size(), stops.back().size) : 0;
    ans += increasing.size() - size_indicator;
    stops.push_back({A[i], increasing.size()});
    increasing.push_back(A[i]);
  }
  return ans;
}

int main() {
  vector<int> A = read();
  chrono::time_point<chrono::system_clock> start, end;
  size_t elapsed_time;

  start = chrono::system_clock::now();
  printf("O(n*lg(n)) %lu\n", SpecialPairsCounter()(A));
  end = chrono::system_clock::now();
  elapsed_time = chrono::duration_cast<chrono::milliseconds>
    (end-start).count();
  printf(" Ejecución en: %lu ms\n", elapsed_time);

  start = chrono::system_clock::now();
  printf("O(n)       %lu\n", countSpecialN(A));
  end = chrono::system_clock::now();
  elapsed_time = chrono::duration_cast<chrono::milliseconds>
    (end-start).count();
  printf(" Ejecución en: %lu ms\n", elapsed_time);

  return 0;
}

